<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formHandler extends Controller
{
   public function store(Request $request){

       $validated = $request->validate([
           'full_name' => 'required|min:3|max:255',
           'email' => 'email:rfc,dns',

       ]);
       $data['name'] =$request->full_name;
       $data['email'] =$request->email;
       $data['lines'] = file($request->file);
       $lines= file($request->file);
       foreach ($lines as $line_num => $line) {
           $line = substr($line, 0, -1);;
           $exploded=explode(';',$line);
           $data['exploded'] =$exploded;
        break;
       }
return view('output',$data);

   }
}
