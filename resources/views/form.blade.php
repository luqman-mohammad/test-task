<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Task</title>
</head>
<body>
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger m-2">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{url('csv-parser')}}"  enctype="multipart/form-data">
        @csrf
    <div class="row">
    <div class="mb-3">
        <label for="name_label" class="form-label mt-4">Full Name</label>
        <input type="text" name="full_name" class="form-control" id="name_label"  required autofocus>
    </div>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label" >Email address</label>
        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="luqman@example.com" required>
    </div>
    </div>
    <div class="mb-3">
        <label for="formFile" class="form-label">Select CSV File</label>
        <input type="file" name="file" class="form-control" type="file" id="formFile" required>
    </div>
    <div class="col-auto" style="margin-left: 45%;">
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </div>
    </form>
</div>

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>
