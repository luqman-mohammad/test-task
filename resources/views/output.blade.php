<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Task</title>
</head>
<body>
<div class="container">
<div class="alert alert-info m-2">
    Name : {{$name}} <br>
    Email : {{$email}}
</div>
    <table class="table table-striped">
        <thead>
        <tr>
           @foreach($exploded as   $header)
                <th scope="col">{{$header}}</th>
               @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach ($lines as $line_num => $line)
            @if($line_num > 0)
                @php
                $line = substr($line, 0, -1);
                $exploded2=explode(';',$line);
                @endphp

            <tr>
                @foreach($exploded2 as $line_num =>$data)

            <th scope="row">{{$data}}</th>
                @endforeach
        </tr>
@endif
        @endforeach
        </tbody>
    </table>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</div>
</body>
</html>
